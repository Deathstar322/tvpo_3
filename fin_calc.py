import numpy as np

# S = int(input("Введите сумму заёма:"))
# m = int(input("Введите кол-во месяцев:"))
# x = int(input("Введите процент годовых:"))
S = 1000
m = 12
x = 10

# комментарий


def pay(S, m, x):
    Ss = S  # Запоминаем изначальную сумму долга
    pereplata = 0  # Сумма всех начислениий с процентов
    ANSWER = np.zeros((m, 5))  # Создаём пустой массив
    r = (x / 100) / 12  # Определеяем % месячных
    A = S * (r + r / ((1 + r) ** m - 1))  # Определяем сумму месячного платежа
    print("| № платежа | Задолженность по кредиту | Начисленные проценты| Основной долг | Сумма платежа |")
    for k in range(m):
        rm = S * r  # rm - начисленные проценты
        ANSWER[k][0] = (k + 1)
        ANSWER[k][1] = S
        ANSWER[k][2] = rm
        ANSWER[k][3] = (A - rm)
        ANSWER[k][4] = A
        pereplata = pereplata + ANSWER[k][2]
        S = S - (A - rm)  # Задолженность по кредиту
        print("|{:<11.0f}|{:<26.2f}|{:<22.2f}|{:<15.2f}|{:<15.2f}|".format(ANSWER[k][0], ANSWER[k][1], ANSWER[k][2],
                                                                           ANSWER[k][3], ANSWER[k][4]))
    print("_" * 95)
    print("|{:<11}" "{:<27}|{:<22.2f}|{:<15.2f}|{:<15.2f}|".format("Итого:", "", pereplata, Ss, Ss + pereplata))
    return [pereplata, Ss, Ss + pereplata]


print("Сумма долга: " + str(S))

print("Количество месяцев: " + str(m))

print("Процент годовых: " + str(x))

print("_" * 95)
print(pay(S, m, x))

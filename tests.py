from fin_calc import pay


def test_pay():
    assert pay(1000, 12, 10) == [54.99064676011488, 1000, 1054.990646760115]
    assert pay(10000, 6, 10) == [293.68365113550635, 10000, 10293.683651135507]
    assert pay(13000, 6, 5) == [190.24023418310964, 13000, 13190.24023418311]


def test_pay_input_sum():
    try:

        pay("", 6, 5)
        assert False
    except TypeError:
        assert True


def test_pay_input_month():
    try:
        pay(1000, "", 5)
        assert False
    except TypeError:
        assert True


def test_pay_input_percents():
    try:
        pay(1000, 12, "")
        assert False
    except TypeError:
        assert True


def test_pay_big_sum():
    assert pay(5000000, 120, 8) == [2279655.66132139, 5000000, 7279655.66132139]
    assert pay(2000000, 24, 14) == [304618.39688263106, 2000000, 2304618.396882631]


def test_false():
    assert False
